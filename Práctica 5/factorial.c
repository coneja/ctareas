#include <stdio.h>


int factorial(int n){
	int r;
	if(n == 0){                                        //Checa que el numero no sea 0
		return 1;
	}else{
		r = n*factorial(n-1);                                //Calculo del factorial
		return(r);                                             //Devuelve r
	}
}
	
	int main()
		{
			int n, res;
			printf("Dame un numero: ");
			scanf("%d",&n);                                      //Pide el n�mero
			res = factorial(n);									//Lo manda a la funcion factorial
			printf("El factorial de %d es: %d", n, res);       //Imprime
		}
	
		
