#include <stdio.h>
#include <stdlib.h>
#include "sqlite3.h"

int main(int argc, char* argv[])
{
   sqlite3 *db;
   char *error = 0;
   int res;
   char *sql;

   //Abre Base de datos
   res = sqlite3_open("test.db", &db);
   if (res)
     {
       fprintf(stderr, "No se puede abrir la base de datos: %s\n", sqlite3_errmsg(db));
       exit(0);
     }
   else
     {
       fprintf(stderr, "La base de datos esta bien\n");
     }
   //Creacion tabla
   sql = "CREATE TABLE prueba2 ("
     "`fecha` DATETIME, "
     "`numero` NUMBER)";

   /* Execute SQL statement */
   res = sqlite3_exec(db, sql, NULL, 0, &error);
   if (res != SQLITE_OK)
     {
       fprintf(stderr, "Error: %s\n", error);
       sqlite3_free(error);
     }
   else
     {
       fprintf(stdout, "Tabla creada!\n");
     }

   sqlite3_close(db);

   return 0;
}


