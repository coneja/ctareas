#include<stdio.h> 				//Entrada estandar

int main() 					//main
{
	int n;					//declaramos n como variable
	printf("Dame el un número: ");
	scanf("%d",&n);				//lee desde teclado el numero que ingresaremos
	for(int i=0; n>=i;i++)			//contador que llega hasta el número que ingresaremos por teclado
		if(i%2 != 0)			//condicion, si el residuo es diferente de 0
		printf("%d\n",i);		//imprime i
