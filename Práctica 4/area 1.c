#include <stdio.h>


int main()
{
    char opcion;
    int b, h, r; 						//Declaracion de variables, enteras y un char para el case

    do								//Hacer mientras opcion != 4
    {
        printf( "\n   >>> MENU \n<<<" );			//Menu
        printf( "\n\n   1. Area Triangulo");
        printf( "\n   2. Area Rectangulo");
        printf( "\n   3. Area Cuadrado");
        printf( "\n   4. Salir\n" );

            printf( "\n   Introduzca opcion (1-4): ");
            fflush( stdin );
            
	scanf( "%c", &opcion);						//Lee la opcion

        switch ( opcion )						//Case
        {
                      /* Opción 1: Triangulo */
            case '1': printf( "\n   Introduzca la base\n: " );
                      scanf( "%d", &b);
                      printf( "\n   Introduzca la altura\n: " );
                      scanf( "%d", &h);
		      r = (b*h)/2;
                      printf( "\nEl resultado es : %d \n\n", r );
                      break;

                      /* Opción 2: Rectangulo */
            case '2': printf( "\n   Introduzca la base\n: " );
                      scanf( "%d", &b);
                      printf( "\n   Introduzca la altura\n: " );
                      scanf( "%d", &h);
		      r = b*h;
                      printf( "\nEl resultado es : %d \n\n", r );
                      break;

                      /* Opción 3: Cuadrado */
            case '3': printf( "\n   Introduzca el lado\n: " );
                      scanf( "%d", &b);
		      r = b*b;
		      printf( "\nEl resultado es : %d \n\n", r );
                      break;
	
	    default: printf("No mas opciones\n");
	}	

    } while ( opcion != '4' );			
}


