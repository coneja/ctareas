#include<stdio.h>
#include <limits.h>

int main()
{

   printf("Numero de bits en un byte %d\n", CHAR_BIT);

   printf("Minimo valor de SIGNED CHAR = %d\n", SCHAR_MIN);
   printf("Maximo valor de SIGNED CHAR = %d\n", SCHAR_MAX);
   printf("Maximo valor de UNSIGNED CHAR = %d\n", UCHAR_MAX);

   printf("Minimo valor de SHORT INT = %d\n", SHRT_MIN);
   printf("Maximo valor de SHORT INT = %d\n", SHRT_MAX); 

   printf("Minimo valor de INT = %d\n", INT_MIN);
   printf("Maximo valor de  INT = %d\n", INT_MAX);

   printf("Minimo valor de  CHAR = %d\n", CHAR_MIN);
   printf("Maximo valor de  CHAR = %d\n", CHAR_MAX);

   printf("Minimo valor de LONG = %ld\n", LONG_MIN);
   printf("Maximo valor de  LONG = %ld\n", LONG_MAX);
}
