#include<stdio.h>
#include<conio.h>
#define X 20    //numero de filas 
#define Y 20   //numero de columnas 
int main()
{

int i, ii, ia, ib, z, w, v; // 'ia' 'i' = derecha, 'z' 'ii' = arriba
//casillas
//en la casilla que se ve
char vida[X][Y], estado[X][Y];  //tabla que se mostrar�, en la otra se aplicar�n los cambios

for(i=0; i<X; i++) //Creando la matriz
    {
        for(ii=0; ii<Y; ii++)
        {
            vida[i][ii]=' ';
        }
    }
    
    printf("\nJuego de la Vida\n\n");

//estado inicial
   vida[1][1] = vida[0][1]='X';vida[0][4]='X';vida[1][0]='X';vida[2][0]='X';vida[2][4]='X';vida[3][0]='X';vida[3][1]='X';vida[3][2]='X';vida[3][3]='X';
  
//inicio del juego de la vida
    while(1==1)
    {   //muestra la tabla de vida
        for(i=0; i<X; i++)
        {
            for(ii=0; ii<Y; ii++)
            {
                printf("%c",vida[i][ii]);
            }
            printf("\n");
        }
        system("pause");  //hasta que pulsemos una tecla, no mostrar� el siguiente estado de la tabla
        //recorrido total para comprobar si ha llegado al limite de la tabla
        for(i=0; i<X; i++)
        {
            for(ii=0; ii<Y; ii++)
            {
                v=0;

                if(i>=X-1)
                    ia=0;
                else
                    ia=i+1;

                if(ii>=Y-1)
                    w=0;
                else
                    w=ii+1;

                if(i<=0)
                    ib=X-1;
                else
                    ib=i-1;

                if(ii<=0)
                    z=Y-1;
                else
                    z=ii-1;

                //saber si los vecinos est�n vivos o muertos
                if(vida[ia][ii]=='X')    v++;
                if(vida[ib][ii]=='X')    v++;
                if(vida[i][z]=='X')    v++;
                if(vida[i][w]=='X')    v++;
                if(vida[ia][z]=='X')   v++;
                if(vida[ib][z]=='X')   v++;
                if(vida[ia][w]=='X')   v++;
                if(vida[ib][w]=='X')   v++;

                //condicion, saber si casilla vive o muere
                if(vida[i][ii]=='X')
                {
                    //vive
                    if(v<=1 || v>3)
                    {
                        estado[i][ii]=' ';
                    }else{
                            estado[i][ii]='X';
                    }

                }else
                {
                    // muere
                    if(v==3)
                    {
                        estado[i][ii]='X';
                    }else{

                        estado[i][ii]=' ';
                    }
                }

            }

        } 

        //guardamos la tabla estado en la tabla vida, para que la pr�xima vez que entre en el bucle se muestre actualizada
        for(i=0; i<X; i++)
        {
            for(ii=0; ii<Y; ii++)
            {
                vida[i][ii]=estado[i][ii];
            }
        }  
    } 

}
